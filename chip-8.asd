(defsystem "chip-8"
  :version "0.1.0"
  :author "daboddo"
  :license ""
  :depends-on (:alexandria
	       :arrow-macros)
  :components ((:module "src"
                :components
                ((:file "package")
		 (:file "main")
		 (:file "cpu"))))
  :description "Steve Losh's CHIP-8 project tutorial"
  :in-order-to ((test-op (test-op "chip-8/tests"))))

(defsystem "chip-8/tests"
  :author ""
  :license ""
  :depends-on ("chip-8"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for chip-8"
  :perform (test-op (op c) (symbol-call :rove :run c)))
