(defpackage chip-8/tests/main
  (:use :cl
        :chip-8
        :rove))
(in-package :chip-8/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :chip-8)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
