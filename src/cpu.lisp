(in-package :chip-8)
;; GLOBAL VARIABLES ;;

(defparameter *c* nil)
(defparameter delay-timer nil)
(defparameter sound-timer nil)

;; TYPES ;;

(deftype int4 () '(unsigned-byte 4))
(deftype int8 () '(unsigned-byte 8))
(deftype int12 () '(unsigned-byte 12))
(deftype int16 () '(unsigned-byte 16))

;; CONSTANTS ;;

(defconstant +memory-size+ 4096) ; 4KB of main memory
(defconstant +cycles-per-second+ 500)
(defconstant +cycles-before-sleep+ 10)

(defconstant +screen-width+ 64)
(defconstant +screen-height+ 32)

;; STRUCTURES ;;

(defstruct chip
  (running t :type boolean)
  (registers (make-array 16 :element-type 'int8)
   :type (simple-array int8 (16))
   :read-only t)
  (index 0 :type int16)
  (pc #x200 :type int12)
  (stack (make-array 16 :element-type 'int12 :fill-pointer 0)
   :type (vector int12 16)
   :read-only t)
  (memory (make-array +memory-size+ :element-type 'int8)
   :type (simple-array int8 (#.+memory-size+))
   :read-only t)
  (video (make-array (* +screen-width+ +screen-height+) :element-type 'fixnum)
         :type (simple-array fixnum (#.(* +screen-height+ +screen-width+))))
  (loaded-rom nil :type (or null string)))


;; MACROS & GENERAL FUNCTIONS ;;

;;; defun-inline macro from Steve Losh
(defmacro defun-inline (name args &body body)
  `(progn
     (declaim (inline ,name))
     (defun ,name ,args ,@body)
     ',name))

(defmacro with-chip ((chip) &body body)
  `(with-accessors ((running chip-running)
		    (registers chip-registers)
		    (index chip-index)
		    (pc chip-pc)
		    (stack chip-stack)
		    (memory chip-memory)
		    (loaded-rom chip-loaded-rom)
		    (flag chip-flag))
       ,chip
     ,@body))

;;; zapf macro from Steve Losh
(defmacro zapf (place expr)
  (multiple-value-bind
        (temps exprs stores store-expr access-expr)
      (get-setf-expansion place)
    `(cl::let* (,@(mapcar #'list temps exprs)
            (,(car stores)
             (let ((% ,access-expr))
               ,expr)))
      ,store-expr)))

(defun parse-instruction-argument-bindings (argument-list)
  (flet ((normalize-arg (args)
	   (destructuring-bind (symbol &optional (nibbles 1))
	       (alexandria:ensure-list args)
	     (list symbol nibbles))))
    (loop
      for (symbol nibbles) in (mapcar #'normalize-arg argument-list)
      for position = 3 then (- position nibbles)
      when (not (eql symbol '_))
      collect `(,symbol (ldb (byte ,(* nibbles 4)
				   ,(* position 4))
			     instruction)))))

(defmacro define-instruction (name argument-list &body body)
  `(progn
     (declaim (ftype (function (chip int16) null) ,name))
     (defun ,name (chip instruction)
       (declare (ignorable instruction))
       (with-chip (chip)
	 (macrolet ((register (index) `(aref registers ,index)))
           (let ,(parse-instruction-argument-bindings argument-list)
	     ,@body)))
       nil)))

(defun-inline chip-flag (chip)
  (aref (chip-registers chip) #xF))

(defun-inline (setf chip-flag) (new-value chip)
  (setf (aref (chip-registers chip) #xF) new-value))

(defun-inline chop (bits integer)
  (ldb (byte bits 0) integer))

(defun-inline cat-bytes (msb lsb)
  (dpb msb (byte 8 8) lsb))

(defun-inline digit (position integer &optional (base 10))
  (-<> integer
    (floor <> (expt base position))
    (mod <> base)))

(defun-inline +_8 (x y)
  (let ((result (+ x y)))
    (values (chop 8 result)
	    (if (> 255 result) 1 0))))

(defun-inline -_8 (x y)
  (let ((result (- x y)))
    (values (chop 8 result)
	    (if (> x y) 1 0))))

(defun-inline get-bit (v position)
  (ldb (byte 1 position) v))

(defun-inline >>_8 (v)
  (values (ash v -1)
          (get-bit 0 v)))

(defun-inline <<_8 (v)
  (values (chop 8 (ash v 1))
          (get-bit 7 v)))


;; INFRASTRUCTURE ;;

(defun reset (chip)
  (with-chip (chip)
    (fill registers 0)
    (fill memory 0)
    (replace memory (alexandria:read-file-into-byte-vector loaded-rom)
	     :start1 #x200)
    (setf running t
	  pc #x200
	  (fill-pointer stack) 0))
  (values))

(defun load-rom (chip filename)
  (setf (chip-loaded-rom chip) filename)
  (reset chip))

(defun emulate-cycle (chip)
  (with-chip (chip)
    (let ((instruction (cat-bytes (aref memory pc)
				  (aref memory (+ pc)))))
      (zapf pc (chop 12 (+ % 2)))
      (dispatch-instruction chip instruction))))

(defun dispatch-instruction (chip instruction)
  (macrolet ((call (name) `(,name chip instruction)))
    (ecase (logand #xF000 instruction)
      (#x0000 (ecase instruction
		(#x00E0 (call op-cls))
		(#x00EE (call op-ret))))
      (#x1000 (call op-jp-imm))
      (#x2000 (call op-call))
      (#x3000 (call op-se-reg-imm))
      (#x4000 (call op-sne-reg-imm))
      (#x5000 (ecase (logand #x000F instruction)
		(#x0 (call op-se-reg-reg))))
      (#x6000 (call op-ld-reg<imm))
      (#x7000 (call op-add-reg<imm))
      (#x8000 (ecase (logand #x000F instruction)
		(#x0 (call op-ld-reg<reg))
		(#x1 (call op-or))
		(#x2 (call op-and))
		(#x3 (call op-xor))
		(#x4 (call op-add-reg<reg))
		(#x5 (call op-sub-reg<reg))
		(#x6 (call op-shr))
		(#x7 (call op-subn-reg<reg))
		(#xE (call op-shl))))
      (#x9000 (ecase (logand #x000F instruction)
		(#x0 (call op-sne-reg-reg))))
      (#xA000 (call op-ld-i<imm))
      (#xB000 (call op-jp-imm+reg))
      (#xC000 (call op-rnd))
      (#xD000 (call op-drw))
      (#xE000 (ecase (logand #x00FF instruction)
		(#x9E (call op-skp))
		(#xA1 (call op-sknp))))
      (#xF000 (ecase (logand #x00FF instruction)
		(#x07 (call op-ld-reg<dt))
		(#x0A (call op-ld-reg<key))
		(#x15 (call op-ld-dt<reg))
		(#x18 (call op-ld-st<reg))
		(#x1E (call op-add-index<reg))
		(#x29 (call op-ld-font<vx))
		(#x33 (call op-ld-bcd<vx))
		(#x55 (call op-ld-mem<regs))
		(#x65 (call op-ld-regs<mem)))))))

(defun run-cpu (chip)
  (loop while (chip-running chip)
	for cycle from 1
	when (= cycle +cycles-before-sleep+)
	do (progn
	     (setf cycle 0)
	     (sleep (/ +cycles-before-sleep+ +cycles-per-second+)))
	do (emulate-cycle chip)))

(defun run (rom-filename)
  (let ((chip (make-chip)))
    (setf *c* chip)
    (load-rom chip rom-filename)
    (run-cpu chip)))

;; INSTRUCTIONS ;;

;;; ARITHMETIC-LOGIC

(define-instruction op-add-reg<reg (_ rx ry)
  "0x8xy4 - ADD Vx, Vy: set Vx = Vx + Vy; VF = carry"
  (setf (values (register rx) flag)
	(+_8 (register rx) (register ry))))

(define-instruction op-add-reg<imm (_ rd (imm 2))
  "0x7xkk - ADD Vx byte: set Vx = Vx + kk"
  (zapf (register rd) (+_8 % imm)))

(define-instruction op-add-index<reg (_ r)
  "0xFx1E- ADD I, Vx: set I = I + Vx"
  (zapf index (+_8 % (register r))))

(define-instruction op-sub-reg<reg (_ rx ry)
  "0x8xy5 - SUB Vx, Vy: set Vx = Vx - Vy; VF = NOT borrow"
  (setf (values (register rx) flag)
	(-_8 (register rx) (register ry))))

(define-instruction op-subn-reg<reg (_ rx ry)
  "0x8xy7 - SUBN Vx, Vy: set Vx = Vy - Vx; VF = NOT borrow"
  (setf (values (register rx) flag)
        (-_8 (register ry) (register rx))))

(define-instruction op-shr (_ r)
  "0x8xy6 - SHR Vx: set Vx = Vx >> 1"
  (setf (values (register r) flag)
        (>>_8 (register r))))

(define-instruction op-shl (_ r)
  "0x8xy6 - SHL Vx: set Vx = Vx << 1"
  (setf (values (register r) flag)
        (<<_8 (register r))))

(define-instruction op-or (_ rd rf _)
  "0x8xy1 - OR Vx, Vy: set Vx = Vx OR Vy"
  (zapf (register rd) (logior % (register rf))))

(define-instruction op-and (_ rd rf _)
  "0x8xy2 - AND Vx, Vy: set Vx = Vx AND Vy"
  (zapf (register rd) (logand % (register rf))))

(define-instruction op-xor (_ rd rf _)
  "0x8xy3 - XOR Vx, Vy: set Vx = Vx XOR Vy"
  (zapf (register rd) (logxor % (register rf))))

(define-instruction op-rnd (_ rd (mask 2))
  "0xCxkk - RND: set Vx = random byte & kk"
  (setf (register rd)
        (logand (random 256) mask)))

;;; LOADS

(define-instruction op-ld-i<imm (_ (val 3))
  "0xAnnn - LD I, addr: set I = nnn"
  (setf index val))

(define-instruction op-ld-reg<imm (_ r (imm 2))
  "0x6xkk - LD Vx, byte: set Vx = kk"
  (setf (register r) imm))

(define-instruction op-ld-reg<reg (_ rx ry _)
  "0x8xy0 - LD Vx, Vy: set Vx = Vy"
  (setf (register rx) (register ry)))

(define-instruction op-ld-reg<dt (_ r _ _)
  "0xFx07 - LD Vx, DT: set Vx = DT"
  (setf (register r) delay-timer))

(define-instruction op-ld-dt<reg (_ r _ _)
  "0xFx15 - LD Vx, DT: set DT = Vx"
  (setf delay-timer (register r)))

(define-instruction op-ld-st<reg (_ r _ _)
  "0xFx18 - LD Vx, ST: set ST = Vx"
  (setf sound-timer (register r)))

(define-instruction op-ld-mem<regs (_ n _ _)
  "0xFx55 - LD [I], Vx: Set [I..x] to V0..Vx"
  (replace memory registers :start1 index :end2 (1+ n)))

(define-instruction op-ld-regs<mem (_ n _ _)
  "0xFx65 - LD [I], Vx: Set V0..Vx to [I..x]"
  (replace registers memory :end1 (1+ n) :start2 index))

;;; FLOW CONTROL

(define-instruction op-se-reg-imm (_ r (imm 2))
  "0x3xkk - SE Vx, byte = set PC += 2 if Vx == kk"
  (when (= (register r) imm)
    (incf pc 2)))

(define-instruction op-sne-reg-imm (_ r (imm 2))
  "0x4xkk - SE Vx, byte = set PC += 2 if Vx != kk"
  (when (not (= (register r) imm))
    (incf pc 2)))

(define-instruction op-se-reg-reg (_ rx ry)
  "0x5xy0 - SE Vx, Vy = set PC += 2 if Vx == Vy"
  (when (= (register rx) (register ry))
    (incf pc 2)))

(define-instruction op-sne-reg-reg (_ rx ry)
  "0x9xy0 - SE Vx, Vy = set PC += 2 if Vx != Vy"
  (when (not (= (register rx) (register ry)))
    (incf pc 2)))

(define-instruction op-jp-imm (_ (addr 3))
  "0x1nnn - JP addr: jump to nnn"
  (setf pc addr))

(define-instruction op-jp-imm+reg (_ (addr 3))
  "0xBnnn - JP V0, addr: jump to V0+nnn"
  (setf pc (chop 12 (+ addr (register 0)))))

(define-instruction op-call (_ (addr 3))
  "0x2nnn - CALL addr: call subroutine at nnn"
  (vector-push pc stack)
  (setf pc addr))

(define-instruction op-ret ()
  "0x00EE - RET: return from subroutine"
  (setf pc (vector-pop stack)))

;;; MEMORY

(define-instruction op-ld-bcd<vx (_ r)
  "0xFx33 - LD B, Vx: Store BCD representation of Vx in memory locations I, I+1 and I+2"
  (let ((number (register r)))
    (setf (aref memory (+ index 0)) (digit 2 number)
          (aref memory (+ index 1)) (digit 1 number)
          (aref memory (+ index 2))  (digit 0 number))))
